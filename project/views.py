from django.shortcuts import render
# from project.forms import registerform
from django.contrib.auth import login,authenticate,logout
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.models import User
from project.models import register,Post,activity,education
import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password


# Create your views here.
@login_required
def about(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]


    return render(request,'about.html',{'profile':profile,'act':activities})

@login_required
def album(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]
    all = Post.objects.filter(user=reg).order_by('-id')


    return render(request,'album.html',{'profile':profile,'act':activities,'all':all})

def contact(request):
    return render(request,'contact.html')

@login_required
def dashboard(request):
    reg = register.objects.get(user__username=request.user.username)
    all = Post.objects.all().order_by('-id')
    if request.method=='POST':
        if 'postupload' in request.POST:
            cont = request.POST['content']
            med = request.FILES['media']

            post_obj = Post(user=reg,content=cont,media=med)
            post_obj.save()
            return HttpResponseRedirect('/project/timeline')

    return render(request,'dashboard.html',{'profile':reg,'all':all})

def frontpage(request):
    if request.method=='POST':
        if 'signupbtn' in request.POST:
            fsname = request.POST['First-name']
            lsname = request.POST['Last-name']
            email = request.POST['Email']
            password = request.POST['Password']
            dob = request.POST['dob']
            gender = request.POST['gender']

            user = User.objects.create_user(email,email,password)
            user.first_name = fsname
            user.last_name = lsname
            user.save()

            rg = register(user=user,date_of_birth=dob,gender=gender)
            rg.save()
            msz = 'Dear {} your Account created successfully'.format(fsname)
            return render(request,'frontpage1.html',{'status':msz}) 
    return render(request,'frontpage1.html')

@login_required
def settings_account(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]

    return render(request,'settings_account.html',{'profile':profile,'all':all,'act':activities})

@login_required
def settings_basicinfo(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]
    if request.method == "POST":
        fs = request.POST['first']
        ls = request.POST['last']
        em = request.POST['email']
        # us = request.POST['username']
        cont = request.POST['contact']
        gen = request.POST['optradio']
        dob = request.POST['date']
       
        user.first_name=fs
        user.last_name =ls
        user.email = em
        # user.username = us
        user.save()
        profile.contact = cont
        profile.gender = gen
        profile.date_of_birth=dob
        profile.save()

        if "image" in request.FILES:
            img = request.FILES['image']
            profile.profile_pic = img
            profile.save()
        
        if "image1" in request.FILES:
            img1 = request.FILES['image1']
            profile.cover_pic = img1
            profile.save()

        return render(request,'settings_basicinfo.html',{'data':user,'profile':profile,'status':'Changes Saved Successfully!!','c':'success'})
    return render(request,'settings_basicinfo.html',{'profile':profile,'act':activities})

@login_required
def settings_education(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=profile).order_by('-id')[:5]
    edu=''
    check = education.objects.filter(user=profile)
    if len(check)>=1:
        edu = education.objects.get(user=profile)
    else:
        edu = education(user=profile)
    if request.method == "POST":
        comp = request.POST['company']
        pos = request.POST['position']
        w_city = request.POST['work_c']
        w_from = request.POST['work_f']
        w_to = request.POST['work_t']
        uni = request.POST['university']
        u_city = request.POST['uni_c']
        u_from = request.POST['uni_f']
        u_to = request.POST['uni_t']
        streem = request.POST['stream']
        sch = request.POST['school']
        s_city = request.POST['school_c']
        s_from = request.POST['school_f']
        s_to = request.POST['school_t']
        
        edu.save()
        edu.company = comp
        edu.position = pos
        edu.work_city = w_city
        edu.work_from = w_from
        edu.work_to = w_to
        edu.university = uni
        edu.uni_city = u_city
        edu.uni_from = u_from
        edu.uni_to = u_to
        edu.stream = streem
        edu.school = sch
        edu.school_city = s_city
        edu.school_from = s_from
        edu.school_to = s_to
        edu.save()
        return render(request,'settings_education.html', {'profile':profile,'status':'Changes Saved Successfully!!','c':'success'})
    return render(request,'settings_education.html', {'profile':profile,'act':activities,'edu':edu})

@login_required
def settings_interests(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]

    return render(request,'settings_interests.html',{'profile':profile,'act':activities})

@login_required
def settings_password(request):
    user = User.objects.get(username=request.user.username)
    login_user_password = request.user.password
    profile = register.objects.get(user__username=request.user.username)
    reg = register.objects.get(user__username=request.user.username)
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]
    if request.method == 'POST':
        current = request.POST['old']
        newpas = request.POST['new']

        check = check_password(current,login_user_password)
        if check==True:
            user.set_password(newpas)
            user.save()
            return render(request,'settings_password.html',{'status':'Password Change Successfully!!','col':'success'})
        else:
            return render(request,'settings_password.html',{'status':'Incorrect Current Password!!','col':'danger'})
    return render(request,'settings_password.html',{'profile':profile,'act':activities})

def uslogin(request):
    if request.method == 'POST':
        usname = request.POST['Email']
        pwd = request.POST['Password']
        print('User = ',usname)
        user = authenticate(username=usname,password=pwd)
        if user:
            if user.is_staff:
                login(request,user)
                response = HttpResponseRedirect('/project/dashboard')
                response.set_cookie('username',usname)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            elif user.is_active:
                login(request,user)
                response= HttpResponseRedirect('/project/dashboard')
                response.set_cookie('username',usname)
                response.set_cookie('id',user.id)
                response.set_cookie('logintime',datetime.datetime.now())
                return response
            
        else:
            return render(request,'frontpage1.html',{'status': 'Invalid User Details'})
    return render(request,'frontpage1.html')

@login_required
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/project/uslogin')
    response.delete_cookie('id')
    return response

@login_required
def timeline(request):
    reg = register.objects.get(user__username=request.user.username)
    all = Post.objects.filter(user=reg).order_by('-id')
    activities = activity.objects.filter(user=reg).order_by('-id')[:5]
    if request.method == 'POST':
        if 'postupload' in request.POST:
            cont = request.POST['content']
            med = request.FILES['media']

            post_obj = Post(user=reg,content=cont,media=med)
            post_obj.save()
            act = "{} has upload a post".format(reg.user.first_name)
            # like = "{} has liked a Photo".format(reg.user.first_name)
            # comment = "{} has commented on Photo".format(reg.user.first_name)
            obj = activity(user=reg,content=act)
            obj.save()
            return HttpResponseRedirect('/project/timeline')

    return render(request,'timeline.html',{'profile':reg,'all':all,'act':activities})

@login_required
def find_friend(request):
    return render(request,'find friend.html')

def friends(request):
    user=User.objects.get(username=request.user.username)
    profile = register.objects.get(user__username=request.user.username)
    return render(request,'friends.html',{'profile':profile})
