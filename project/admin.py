from django.contrib import admin
from project.models import register,Post,activity,education


# Register your models here.
admin.site.register(register)
admin.site.register(Post)
admin.site.register(activity)
admin.site.register(education)
