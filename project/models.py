from django.db import models
from django.contrib.auth.models import User

class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    date_of_birth = models.CharField(max_length=100,blank=True,)
    gender = models.CharField(max_length=100)
    contact=models.IntegerField(blank=True, null=True)
    about=models.CharField(max_length=300, blank=True, null=True)
    profile_pic = models.ImageField(upload_to='images/%Y/%m/%d',blank=True,null=True)
    cover_pic = models.ImageField(upload_to='images/%Y/%m/%d',blank=True, null=True)
    registred_on = models.DateTimeField(auto_now_add=True)
    changed_on = models.DateField(auto_now=True)

    def __str__(self):
        return self.user.first_name


class Post(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    content = models.TextField(blank=True)
    media = models.FileField(upload_to='posts/%Y/%m/%d',blank=True)
    upload_on = models.DateTimeField(auto_now_add = True)
    edit_on = models.DateField(auto_now=True)

    def __str__(self):
        return self.user.user.username

class activity(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    content = models.TextField(blank=True)
    status = models.BooleanField(default=False)
    time = models.TimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.user.user.first_name

class education(models.Model):
    user = models.ForeignKey(register, on_delete = models.CASCADE)
    company = models.CharField(max_length=200, blank=True)
    position = models.CharField(max_length=200, blank=True)
    work_city = models.CharField(max_length=200, blank=True)
    work_from = models.CharField(max_length=200, blank=True)
    work_to = models.CharField(max_length=200, blank=True)
    university = models.CharField(max_length=200, blank=True)
    stream = models.CharField(max_length=200, blank=True)
    uni_city = models.CharField(max_length=200, blank=True)
    uni_from = models.CharField(max_length=200, blank=True)
    uni_to = models.CharField(max_length=200, blank=True)
    school = models.CharField(max_length=200, blank=True)
    school_city = models.CharField(max_length=200, blank=True)
    school_from = models.CharField(max_length=200, blank=True)
    school_to = models.CharField(max_length=200, blank=True)


    def __str__(self):
        return self.user.user.first_name