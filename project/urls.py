from django.urls import path
from project import views

app_name = 'project'


urlpatterns = [
    path('dashboard/',views.dashboard,name='dash'),
    path('about/',views.about,name='abo'),
    path('album/',views.album,name='alb'),
    path('contact/',views.contact,name='con'),
    path('s_account/',views.settings_account,name='account'),
    path('s_basicinfo/',views.settings_basicinfo,name='basic'),
    path('s_education/',views.settings_education,name='education'),
    path('s_interest/',views.settings_interests,name='interest'),
    path('s_password/',views.settings_password,name='password'),
    path('timeline/',views.timeline,name='time'),
    path('uslogin/',views.uslogin,name='login'),
    path('uslogout/',views.uslogout,name='logout'),
    path('find_friend/',views.find_friend,name='find_friend'),
    path('friends/',views.friends,name='friends'),
    # path('mypost/',views.mypost,name='mypost'),
]
    